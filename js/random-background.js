var generateRandomHex = () => {
    var hex = [1,2,3,4,5,6,7,8,9,0,'a', 'b', 'c', 'd', 'e', 'f'];
    var color = '#';
    
    for(let i = 0; i < 6; i++) {
        var index = Math.floor(Math.random() * hex.length);
        color += hex[index];
    }

    return color;
}

var changeColor = () => {
    var $text = $('.hex-color');
    var color = generateRandomHex();
    $text.text(color).css('color', color);
}

var attachEvents = () => {
    var $cta = $('.cta');

    $cta.on('click', () => {
        changeColor();
    });
}

$(() => {
    attachEvents();
})