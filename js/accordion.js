var attackEvents = () => {
    var $toggle = $('.toggle');
    var $container = $('.content-container');
    var $content = $('.content');

    $toggle.on('click', function() {
        var height = $(this).next($container).find($content).outerHeight(true);
        var $icon = $(this).find('.icon');
        console.log($icon)
        
        if($(this).next($container).height() > 0) {
            $icon.removeClass('active');
            $(this).next($container).css({
                "height": 0,
            })
        } else {
            $icon.addClass('active');
            $(this).next($container).css({
                "height": height
            })
        }
    })
}

$(() => {
    attackEvents();
})