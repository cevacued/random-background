var calcNrOfSlides = ($slide) => {
    var nrOfSlides = -1;

    $slide.each(() => {
        nrOfSlides++;
    })

    return nrOfSlides;
}

var nextSlide = ($slide, translateValue) => {
    $slide.css("transform", "translateX(" + translateValue + "rem)");
}

var prevSlide = ($slide, translateValue) => {
    $slide.css("transform", "translateX(" + translateValue + "rem)");
}

var attachEvents = () => {
    var $slide = $('.slide');
    var $prevButton = $('.button-prev');
    var $nextButton = $('.button-next');
    var nrOfSlides = calcNrOfSlides($slide);
    var initialTranslateValue = $slide.outerWidth() / 10;
    var translateValue = 0;
    var maxTranslateValue = 0;
    var minTranslateValue = -(nrOfSlides * initialTranslateValue);
   
    $prevButton.on('click', () => {
        if(translateValue == maxTranslateValue) {
            translateValue = minTranslateValue;
            prevSlide($slide, translateValue);
        } else {
            translateValue = parseFloat((translateValue + initialTranslateValue).toFixed(2));
            prevSlide($slide, translateValue);
        }
    })

    $nextButton.on('click', () => {
        if(translateValue == minTranslateValue) {
            translateValue = maxTranslateValue;
            nextSlide($slide, translateValue);
        } else {
            translateValue = parseFloat((translateValue - initialTranslateValue).toFixed(2));
            nextSlide($slide, translateValue);
        }
    })
}

$(() => {
    attachEvents();   
})

//for responsive WIP
//works fine when resize THEN use buttons
//don't work as espected when use buttons THEN resize
// (the slides will have initial width before the resize after the use of buttons)

$(window).resize(() => {
    attachEvents();   
});