// START REQUEST / CREATE WEATHER DATA ------------

var setItemLocalStorage = (item) => {
    $.ajax({
        url: `http://api.weatherapi.com/v1/current.json?key=53a6610aad494a5fae0184219202211&q=${item}`,
    })
    .done((data) => {
        var weatherData = {
            city:       data.location.name,
            region:     data.location.region,
            country:    data.location.country,
            localTime:  data.location.localtime,
            tempC:      data.current.temp_c,
            tempF:      data.current.temp_f,
            icon:       data.current.condition.icon
        }

        var index = window.localStorage.length;
        window.localStorage.setItem(index, JSON.stringify(weatherData));
    })
}

var deleteLocalStorage = () => {
    window.localStorage.clear();
}

var setDefaultLocalStorage = () => {
    deleteLocalStorage();
    var cityArray = [
        "aiud",
        "London",
        "PAKISTAN",
        "PaRiS",
        "Chicago",
        "Osaka",
    ];

    for(var i = 0; i < cityArray.length; i++) {
        setItemLocalStorage(cityArray[i]);
    }
}

var getLocalStorage = () => {
    var storageLength = JSON.parse(window.localStorage.length);
    var storageArray = [];

    for(var i = 0; i < storageLength; i++) {
        storageArray.push((JSON.parse(localStorage.getItem(i))));
    } 

    return storageArray;
}

var weatherTemplate = (weatherData) => {
    return `
    <div class="weather-component">
        <div class="wrapper">
            <div class="header">
                <h3 class="title">${weatherData.city}</h3>
                <span class="time">${weatherData.localTime}</span>
            </div>
            <span class="icon-wrapper"><img src="${weatherData.icon}" class="icon"></span>
            <div class="body">
                <div class="detail-wrapper">
                    <span class="country">${weatherData.country}</span>
                    <span class="region">${weatherData.region}</span>
                </div>
            </div>
            <div class="footer">
                <span class="temperature">${weatherData.tempC} °C</span>
                <span class="temperature">${weatherData.tempF} °F</span>
            </div>
        </div>
    </div>
    `
}

var addToList = () => {
    var $weatherList = $('.weather-list-component');
    var storageLength = getLocalStorage().length;
    
    for(var i = 0; i < storageLength; i++) {
        $weatherList.prepend(weatherTemplate(getLocalStorage()[i]));
    }
}

var addToListSearch = (item) => {
    var $satelliteResponse = $('.satellite.response');
    var $satelliteRequest = $('.satellite.request');

    $satelliteRequest.hide();
    $satelliteResponse.show();

    $.ajax({
        url: `http://api.weatherapi.com/v1/current.json?key=53a6610aad494a5fae0184219202211&q=${item}`,
    })
    .done((data) => {
        var weatherData = {
            city:       data.location.name,
            region:     data.location.region,
            country:    data.location.country,
            localTime:  data.location.localtime,
            tempC:      data.current.temp_c,
            tempF:      data.current.temp_f,
            icon:       data.current.condition.icon
        }
        
        var $weatherList = $('.weather-list-component');
        var lastIndex = window.localStorage.length;
        window.localStorage.setItem(lastIndex, JSON.stringify(weatherData));
        $weatherList.prepend(weatherTemplate(JSON.parse(window.localStorage.getItem(lastIndex))));
        $satelliteResponse.hide();
        $satelliteRequest.show();
    })
}

// END REQUEST / CREATE WEATHER DATA ------------

// START GEOLOCATION ----------------------------

var getCurrentLocation = () => {
    if('geolocation' in navigator) {
        navigator.geolocation.getCurrentPosition((position) => {
            var currentLocation = {
                latitude: position.coords.latitude.toFixed(4),
                longitude: position.coords.longitude.toFixed(4)
            };

            $.ajax({
                url: `http://api.weatherapi.com/v1/current.json?key=53a6610aad494a5fae0184219202211&q=${currentLocation.latitude},${currentLocation.longitude}`,
            })
            .done((data) => {
                var weatherData = {
                    city:       data.location.name,
                    country:    data.location.country,
                    localTime:  data.location.localtime,
                    tempC:      data.current.temp_c,
                    tempF:      data.current.temp_f,
                    icon:       data.current.condition.icon
                };
                var component = $('.hero-component');
                var city = component.find('.city');
                var country = component.find('.country');
                var localTime = component.find('.time');
                var tempC = component.find('.temperature.primary');
                var tempF = component.find('.temperature.secondary');
                
                city.text(weatherData.city);
                country.text(weatherData.country);
                country.text(weatherData.country);
                localTime.text("last updated: " + weatherData.localTime);
                tempC.text(weatherData.tempC + " °C");
                tempF.text(weatherData.tempF + " °F");
            });
          });
      } else {
        console.log("Geolocation is not available for your browser");
    }
}

// END GEOLOCATION ----------------------------

// START WEATHER MODAL --------------------------

var addMask = () => {
    $body = $('body');
    $mask = `<div class="mask"></div>`
    $body.addClass('no-scroll');
    $body.append($mask);
}

var removeMask = ($mask) => {
    $body = $('body');
    $body.removeClass('no-scroll');
    $mask.remove();
}

var displayWeatherModal = (city) => {
    var $component = $('.weather-modal');
    var $condition = $component.find('.condition');
    var $windKph = $component.find('.wind-kph');
    var $windMph = $component.find('.wind-mph');
    var $pressureMb = $component.find('.pressure-mb');
    var $tempC = $component.find('.tempC');
    var $tempF = $component.find('.tempF');
    $component.addClass('active');

    $.ajax({
        url: `http://api.weatherapi.com/v1/forecast.json?key=53a6610aad494a5fae0184219202211&q=${city}&days=4`
    }).done(data => {

        // get current day weather of the selected city
        var current = {
            condition: data.current.condition.text,
            windKph: data.current.wind_kph,
            windMph: data.current.wind_mph,
            pressureMb: data.current.pressure_mb,
            tempC: data.current.temp_c,
            tempF: data.current.temp_f
        }

        $condition.text(current.condition);
        $windKph.text(current.windKph);
        $windMph.text(current.windMph);
        $pressureMb.text(current.pressureMb);
        $tempC.text(current.tempC);
        $tempF.text(current.tempF);

        var days = data.forecast.forecastday;
        var nrOfDays = days.length;
        var forecast = [];
        var forecastDate = [];
        var forecastAvgTempC = [];
        var forecastAvgTempF = [];
        var primaryLineColor = '#2d6cdf'
        var secondaryLineColor = '#f0d146'

        for(let i = 0; i < nrOfDays; i++) {
            var forecastPerDay = {
                date: days[i].date,
                avgTempC: days[i].day.avgtemp_c,
                avgTempF: days[i].day.avgtemp_f
            }
            forecast.push(forecastPerDay);
            forecastDate.push(forecastPerDay.date);
            forecastAvgTempC.push(forecastPerDay.avgTempC);
            forecastAvgTempF.push(forecastPerDay.avgTempF);
        }

        var ctx = $component.find('.chart');
        var chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: forecastDate,
                datasets: [{
                    label: 'Average Temperature (°C)',
                    backgroundColor: 'transparent',
                    borderColor: primaryLineColor,
                    data: forecastAvgTempC
                },
                {   
                    label: 'Average Temperature (°F)',
                    backgroundColor: 'transparent',
                    borderColor: secondaryLineColor,
                    data: forecastAvgTempF
                }]
            },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                callback: function(value, index, values) {
                                    return '°' + value;
                                }
                            }
                        }]
                    }
                }
        }); 
        
        // for testing 
        console.log("Current: ", current);
        console.log("Forecast: ", forecast);
        console.log("Forecast date: ", forecastDate);
        console.log("Forecast avg temp C: ", forecastAvgTempC);
    })
}

var hideWeatherModal = () => {
    var $component = $('.weather-modal');
    $component.removeClass('active');
}

// END WEATHER MODAL ----------------------------

var attachEvents = () => {
    var $searchButton = $('.search .submit');
    
    $searchButton.on('click', () => {
        var searchInput = $('.search .text').val();
        addToListSearch(searchInput);
    })
}

$(() => {

    // on load
    addToList();
    getCurrentLocation();
    attachEvents();

    // OPEN WEATHER MODAL WHEN CLICK ON WEATHER COMPONENT
    $(document).on('click', '.weather-component', (e) => {
        var city = $(e.target.closest('.weather-component')).find('.title').text();
        addMask();
        displayWeatherModal(city);
    })

    // CLOSE WEATHER MODAL WHEN CLICK OUTSIDE OF IT
    $(document).on('click', '.mask', (e) => {
        removeMask($(e.target));
        hideWeatherModal();
    })
})