var add = (val) => {
    val++;
    return val;   
}

var sub = (val) => {
    val--;
    return val;
}

var attachEvents = () => {
    var $ctaAdd = $('.cta-add');
    var $ctaSub = $('.cta-sub');
    var $result = $('.result');

    $ctaAdd.on('click', () => {
        $result.text(add(parseInt($result.text())));
    })

    $ctaSub.on('click', () => {
        $result.text(sub(parseInt($result.text())));
    })
}

$(() => {
    attachEvents();
})