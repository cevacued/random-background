var attachEvents = () => {
    var $toggleTab = $('.toggle-tab');
    var $tab = $('.tab');
    var $content = $('.content');

    $toggleTab.on('click', function() {
        var toggleIndex = $(this).index();
        
        $toggleTab.removeClass('active');
        $(this).addClass('active');
        $tab.removeClass('active');
        
        $tab.each(function() {
            if($(this).index() != toggleIndex) {
                $(this).css({
                    "height": 0
                });
            } else {
                var contentHeight = ($(this).find($content).outerHeight());
                $(this).css({
                    "height": contentHeight
                });
            }
        })
    })
}

$(() => {
    attachEvents();
})